# Rendu "Les droits d’accès dans les systèmes UNIX"

## Binome

- Nom, Prénom, email: Jourdain Lucas lucasjourdain25@gmail.com

- Nom, Prénom, email: Patrick Frank Tchossiewe Djengoue patrick.tchossiewe@gmail.com

##


ssh -i ~/.ssh/instance.pem ubuntu@172.28.101.84

## Question 1

Réponse

sudo adduser toto

sudo adduser toto ubuntu
touch myfile.txt

-r--rw-r-- représente le droits d'accès au fichier.

Tout le monde peut lire le fichier.

Si on souhaite écrire dans ce fichier, il faut appartenir au groupe ubuntu, toto appartient au groupe ubuntu donc il peut écrire dans le fichier.



## Question 2

Réponse

-
Le répertoire est consultable quand il y a un x pour un répertoire.

-

ubuntu:

mkdir mydir
chmod -x mydir
su toto

toto:

cd mydir


-
bash: cd: mydir: Permission denied


-

ubuntu:

chmod u+x mydir
cd mydir
touch data.txt

toto:

ls -al mydir

-
ls: cannot access 'mydir/.': Permission denied
ls: cannot access 'mydir/..': Permission denied
ls: cannot access 'mydir/data.txt': Permission denied
total 0
d????????? ? ? ? ?            ? .
d????????? ? ? ? ?            ? ..
-????????? ? ? ? ?            ? data.txt

-



## Question 3

Réponse

ubuntu:

touch suid.c

echo "" > suid.c  //mettre en les 2 " le code de suid.c
chmod ug+x suid.c


## Question 4

Réponse

## Question 5

Réponse

chfn permet de changer quelques informations que la machine a sur nous.

-rwsr-xr-x 1 root root 85064 May 28  2020 /usr/bin/chfn

Les membre du groupe et les autres peuvent lire et exécuter le fichier.
Le propriétaire du fichier peut lire, écrire dans le fichier.
Tout le monde peut exécuter le fichier avec les privilèges root.



## Question 6

Réponse


Les mots de passe sont stockés dans le fichier /etc/shadow .
par soucis de confidentialite car le fichier /etc/passwd est accessible en lecture a tous les utilisateurs.




## Question 7

Mettre les scripts bash dans le repertoire *question7*.

## Question 8

Le programme et les scripts dans le repertoire *question8*.

## Question 9

Le programme et les scripts dans le repertoire *question9*.

## Question 10

Les programmes *groupe_server* et *groupe_client* dans le repertoire
*question10* ainsi que les tests.
