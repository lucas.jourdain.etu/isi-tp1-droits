import os

euid = os.geteuid()

print("Effective user ID of the current process:", euid)

egid = os.getegid()

print("Effective user ID of the current process:", egid)