#include <stdio.h>

#include <unistd.h>
#include <sys/types.h>




int litfichier(){

  FILE* Fichier; // tu créer un pointeur pour un flux

  Fichier = fopen("mydir/data.txt","r"); // Ouvre Data.txt en lecture seulement et met le flux dans Fichier

  char Buffer[128]; // Créer un tableau de 128 char (une chaine quoi)

  while( fgets(Buffer,128,Fichier) ) // Tant que C pas la fin du fichier, on met 128 char dans Buffer

  printf("%s",Buffer); // puis on affiche a l'écran le Buffer (donc les 128 char)

  fclose(Fichier); // on ferme le fichier (flux)

  return 0;

}


int afficheId(){

//on commence par afficher l'EUID

int euid = geteuid();

printf("%i\n", euid);

//pareil avec EGID

int egid = getegid();

printf("%i", egid);

// Il manque le RUID et le RGID

return 0;

}



int main(int argc, char *argv[])
{
    // TODO
    litfichier();
    afficheId();
    return 0;
}
